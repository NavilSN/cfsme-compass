<?php 

/*
   Template Name: Single Service Page
*/   

get_header(); 
?>    
    <?php  $service_page = get_queried_object(); ?>
    <section id="banner" class="about-us-banner sub-services">
        <div class="container">
            <div class="go-down">
                <a class="go_down_anchor" href="#event-news-list"><img class="animated fadeIn infinite" src="<?php echo get_template_directory_uri(); ?>/img/down-arrow.png" alt=""></a>
            </div>
            <div class="banner_text banner_left_text">
                <h1 class="avenir-bold font-38 font-0d75ad text-uppercase"><?php the_title(); ?> </h1>
                <p class="font-000 avenir-regular font-20">
                <?php if (have_posts()) :
                   while (have_posts()) :
                      the_post();
                         echo wp_trim_words( get_the_content(), 7, '...' ); 
                   endwhile;
                endif; ?></p>
              
            </div>
        </div>
    </section>
    
    
    <section id="event-news-list" class="clearfix">
        <div class="container">
         <div class="row">
            <div class="title_block wow lightSpeedIn">
                <p class="section_subtitle font-18 font-bd8a27 avenir-medium-cond">Services</p>
                <h2 class="section_title avenir-demi font-38 font-ffffff">
                    <span class="first_letter"></span> <?php the_title();  ?>
                </h2>
            </div>
            <div class="list-box">
                <?php 
                    $service_tags = get_the_terms(get_the_ID(), 'services_tags');
                    foreach ($service_tags as $service) {   
                    $thumbnail = get_field('service_image', $service->taxonomy . '_' . $service->term_id);
                ?>
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="img-text">
                        <div class="img-box">

                            <img src="<?php echo $thumbnail ?>" alt="">
                        </div>
                        <div class="overlay-text">
                            <p class="font-14 avenir-regular font-fff"><?php echo $service->name; ?> </p>
                        </div> 
                       <a href="<?php echo site_url() ?>/professional_services/<?php echo $service_page->post_name."/".$service->slug ?>" class="btn-read font-14 avenir-regular font-fff">View</a>   
                    </div>  
                </div>

                <?php  }?>
                 
                 
                  </div>
            
            </div> 
        </div>
    </section>
    

<?php get_footer();?>

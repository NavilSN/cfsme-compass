<?php 

/*
   Template Name: taxonomy-services_categories
*/   

get_header(); 
?>

  <?php  $category = get_queried_object(); ?>
  <section id="banner" class="about-us-banner sub-services">
    <div class="container">
      <div class="go-down">
        <a class="go_down_anchor" href="#event-news-list  "><img class="animated fadeIn infinite" src="<?php echo get_template_directory_uri(); ?>/img/down-arrow.png" alt=""></a>
      </div>
      <div class="banner_text banner_left_text">
        <h1 class="avenir-bold font-38 font-0d75ad text-uppercase"><?php echo $category->name; ?> </h1>
        <p class="font-000 avenir-regular font-20"> <?php echo wp_trim_words( $category->description , 7, '...' ); ?>   </p>
        
      </div>
    </div>
  </section>
    
    <section id="event-news-list" class="clearfix">
        <div class="container">        
         <div class="row">     
          
            <div class="title_block wow lightSpeedIn">
              <p class="section_subtitle font-18 font-bd8a27 avenir-medium-cond">Services</p>
              <h2 class="section_title avenir-demi font-38 font-ffffff"> <span class="first_letter"></span> <?php echo $category->name; ?> </h2>
            </div>
            <div class="list-box clearfix">  
              
              <?php   
              wp_reset_query();
                
              //  GENERAL INSURANCE LOOP  START
              if($category->term_id==11): 
                $args = array('post_type' => 'services',
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'professional_service',
                            'field' => 'slug',
                            'terms' => $category->slug,
                        ),
                    ),
                 );
                $service_loop = new WP_Query($args);
                if($service_loop->have_posts()) {
                  while($service_loop->have_posts()) : $service_loop->the_post();
                    ?>
                      <div class="col-md-3 col-xs-12 col-sm-6">
                        <div class="img-text">
                          <div class="img-box">
                            <img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post_id) ); ?>" alt="">
                          </div>
                          <div class="overlay-text">
                            <p class="font-14 avenir-regular font-fff"> <?php echo  get_the_title(); ?></p>
                          </div> 
                          <a href="<?php the_permalink(); ?>" class="btn-read font-14 avenir-regular font-fff">View  </a>                           
                        <a href="<?php the_permalink(); ?>" class="box-anchor"></a> 
                        </div>
                      </div>
                    <?php endwhile;  
                  } 
                endif; 
                //  GENERAL INSURANCE LOOP END ?>
                
                <?php
                // LIFE INSURANCE LOOP START
                if($category->term_id==13) :    
                  $service_tags = get_the_terms(get_the_ID(), 'services_tags');
                  foreach ($service_tags as $service) {   
                    $thumbnail = get_field('service_image', $service->taxonomy . '_' . $service->term_id); ?>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                      <div class="img-text">
                        <div class="img-box">
                          <img src="<?php echo $thumbnail ?>" alt="">
                        </div>
                        <div class="overlay-text">
                          <p class="font-14 avenir-regular font-fff"><?php echo $service->name ?> </p>
                        </div> 
                        <a href="<?php echo site_url() ?>/professional_services/<?php echo $category->slug."/".$service->slug ?>" class="btn-read font-14 avenir-regular font-fff">View</a> 
                        
                        <a href="<?php echo site_url() ?>/professional_services/<?php echo $category->slug."/".$service->slug ?>" class="box-anchor"></a> 
                    </div>  
                  </div>
                  <?php 
                  } 
                endif; 
                // LIFE INSURANCE LOOP END ?>

              </div>
            </div> 
        </div>
    </section>


<?php get_footer(); ?>



<?php
/**
 * 404 page.
 * 
 */
get_header(); ?>
<section id="newsevent-detail" class="">
    <div class="container">
	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">
            <h1 class="page-title avenir-bold font-0d75ad text-uppercase">Sorry, Page Not Found !</h1>

			<div class="page-wrapper">
				<div class="page-content">
                    <h2 class="section_title avenir-demi font-30 font-343434" style="margin-top:50px;"><span class="first_letter">T</span>his is somewhat embarrassing, isn’t it?</h2>
					<p class="avenir-regular font-20 font-343434" style="margin-top:20px;">It looks like nothing was found at this location. Let's Start Fresh !</p>
					<a href="<?php bloginfo('url'); ?>" class="avenir-demi font-20" style="border:1px solid #0D75AD; padding:5px 20px; margin-top:10px; display:inline-block">Go Home</a>
				</div><!-- .page-content -->
			</div><!-- .page-wrapper -->

		</div><!-- #content -->
	</div><!-- #primary -->
	</div>
</section>	
<?php get_footer(); ?>
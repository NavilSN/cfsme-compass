<?php get_header();?>
<section id="banner" class="about-us-banner news-event-banner">
    <div class="container">
        <div class="go-down">
            <a class="go_down_anchor" href="#about"><img class="animated fadeIn infinite" src="<?php echo get_template_directory_uri(); ?>/img/down-arrow.png" alt=""></a>
        </div>
        <div class="banner_text">
            <h1 class="avenir-bold font-38 font-0d75ad text-uppercase">Latest news</h1>
            <p class="font-000 avenir-regular font-20">How to Choose a Financial Planner?</p>
            <!-- <a href="#" class="banner_find_out_more avenir-regular font-15 font-000">Find Out More</a> -->
        </div>
    </div>
</section>
<section id="newsevent-detail" class="">
    <div class="container">

        <div class="clearfix">


            <div class="title_block wow lightSpeedIn">
                        <p class="section_subtitle font-18 font-bd8a27 avenir-medium-cond">UPDATES</p>
                        <h2 class="section_title avenir-demi font-38 font-ffffff">
                            <span class="first_letter">N</span>ews & Events
                        </h2>
                    </div>



            <div class="detail-box">
                <div class="col-md-9 col-sm-12">
                    <?php
                // Start the Loop.
                while ( have_posts() ) : the_post(); ?>
                        <div class="img-box">
                            <?php the_post_thumbnail(); ?>
                            <div class="detail">
                            <p class="font-22 avenir-demi font-000">
                                <?php the_title(); ?>
                            </p>
<!--                            <p class="font-18 avenir-regular font-000">Capital Business</p>-->
                        </div>
                        </div>
                        
                        <div class="discription-para">
                            <p class="font-16 avenir-demi font-000">
                                <?php the_content(); ?>
                            </p>
                        </div>
                        <?php endwhile;
                        ?>

                </div>


                <div class="col-md-3 col-sm-12">
                    <div class="adderess-box">
                        <h3 class="font-22 avenir-demi font-000">Event Details</h3>
                        <p class="font-18 avenir-demi font-000"><?php the_title(); ?></p>
                        <p class="font-16 avenir-regular font-000"><?php  echo get_field( 'news-events_time', get_the_ID() ); ?></p>
                            <?php $date = new DateTime(get_field( 'news-events_date', get_the_ID() )); ?> 
                        <p class="font-16 avenir-regular font-000"><?php echo $date->format('j - M - Y'); ?></p>
                        <p class="font-16 avenir-regular font-000"><?php  echo get_field( 'news-events_location', get_the_ID() ); ?></p>
                        <h3 class="font-22 avenir-demi font-000">Share</h3>
                        <ul class="social-container">
                            <li class="social-icon">
                                <a href="http://www.facebook.com/sharer.php?u=<?php the_permalink();?>&amp;t=<?php the_title(); ?>" title="Share on Facebook." title="Share on Facebook." target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/facebook-logo-small.png" alt=""></a>
                            </li>
                            <li class="social-icon">
                                <a href="http://www.linkedin.com/shareArticle?mini=true&amp;title=<?php the_title(); ?>&amp;url=<?php the_permalink(); ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/linkedin-logo-small.png" alt="" title="Share on LinkedIn"></a>
                            </li>
                            <li class="social-icon">
                                <a  href="http://twitter.com/home/?status=<?php the_title(); ?> - <?php the_permalink(); ?>" title="Tweet this!" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/twitter-logo-small.png" alt=""></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
<?php get_footer();?>

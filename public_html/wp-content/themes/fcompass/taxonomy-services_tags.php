<?php 
/*
   Template Name: tag service post tag
*/   
get_header(); 
?>

<section id="banner" class="about-us-banner service-banner">
	<div class="container">
		<div class="go-down">
			<a class="go_down_anchor" href="#catering"><img class="animated fadeIn infinite" src="<?php echo get_template_directory_uri(); ?>/img/down-arrow.png" alt=""></a>
		</div>
		<div class="banner_text">
			<h1 class="avenir-bold font-38 font-0d75ad text-uppercase">Here you can see our Solutions</h1>			
		</div>
	</div> 
</section> 
	
	
       
       
       
<!-- Section: Tab Start -->
<section id="catering" class="home-section padding-45 news_events_details">
	<div class="container"> 
    	<div class="row">   
     		      		 	
      		<div class="img-side clearfix">
      		<?php 
		    $parent_post_id = wp_get_post_parent_id( $post->ID); 
		    $parent_post = get_post($parent_post_id);
    		$parent_post_title = $parent_post->post_title;
			?> 
      		<div class="title_block wow lightSpeedIn">
                <h2 class="section_title avenir-demi font-38 font-ffffff"><span class="first_letter"></span><?php echo $parent_post_title; ?> </h2>
            </div> 
				<div id="tab_example">
			  		<div class="col-sm-3 col-md-3 col-xs-12">
						<ul class="tabs" data-persist="true"> 
						 <?php 								
							$terms = get_the_terms( $post->ID, 'services_tags' );		
							$tag = get_queried_object();
							$custom_taxterms = wp_get_object_terms( 
								$post->ID, 'services_tags', 
								array('fields' => 'slug') 
								);					
							// arguments
							$args = array(
								'post_type' => 'services',
								'post_status' => 'publish',
								'posts_per_page' => 50, // you may edit this number
								//'order'=>"DESC",	
								'tax_query' => array(
									array(
										'taxonomy' => 'services_tags',
										'field' => 'slug',
										'terms' => $tag->slug
									)
								),
							);
							
							$postslist = get_posts( $args );							
							foreach ($postslist as $post) :  setup_postdata($post); 
							 ?>	
								
								<li class="ui-state-default <?php if(get_queried_object()->name ==  $post->post_title) echo "ui-tabs-active ui-state-active"; ?>" <?php if($post->ID == 137 || $post->ID==138) echo "style='display:none'"; ?>  >
									<div class="tab_content">
										<div class="tab_one">
											<a href="#view<?php echo $post->ID; ?>">	
											<h4 class="font-16 avenir-regular font-212121;"><?php 
												$title = $post->post_title;
												$tit = strip_tags($title);
												//echo $post->ID;
												echo substr($tit, 0, 180); ?>
											</h4>
											</a>
										</div>
									</div>
								</li>											
							<?php endforeach;  ?>
						</ul>
				</div>
			  	<div class="col-sm-9 col-md-9 col-xs-12">
				  <?php	
						$custom_terms = wp_get_object_terms( $post->ID, 'services_tags', array('fields' => 'slug') );
							// arguments
							$args = array(
								'post_type' => 'services',
								'post_status' => 'publish',
								'posts_per_page' => 50, // you may edit this number	
								'order'=>"DESC",							
								'tax_query' => array(
									array(
										'taxonomy' => 'services_tags',
										'field' => 'slug',
										'terms' => $tag->slug
									)
								)
							);
						$postslist = get_posts( $args );
						// echo "<pre>";
						// print_r($_SESSION);
						// print_r($postslist); die;
						foreach ($postslist as $key => $post) :  setup_postdata($post);
                    if(get_queried_object()->name ==  $post->post_title) {
                        setcookie('miroot7', $key);    
                    }
                    
					?>	
					<div id="view<?php echo $post->ID;?>" class="tabcontents ui-tabs-panel" style="<?php echo get_queried_object()->name ==  $post->post_title ? 'display: block':'display: none'; ?>" aria-hidden="<?php echo get_queried_object()->name ==  $post->post_title ? 'false':'true'; ?>">
					<div class="img-box">
						<div class="img-box " style="background: url(<?php echo the_post_thumbnail_url(); ?>); height: 383px; position: relative;"></div>	
						<div class="detail top-right">
	                   		<p class="font-22 avenir-demi font-212121"><?php echo $post->post_title; ?>
	                   			

	                   		</p>
	                   	</div>	
	                   	</div>				
						<div class="discription-para">
							<p> <?php echo get_the_content(); ?> </p>
						</div>						
					</div>				                               
						<?php endforeach; ?>					
						<?php wp_reset_postdata();  ?>								  
				</div>							
				</div>
	  		</div>
		</div>  
  	</div>		
</section>
<!-- /Section: Tab End -->
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery-ui.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/tabcontent.js"></script>
<script type="text/javascript">
	(function($){		
		$( "#tab_example" ).tabs().addClass( "ui-tabs-vertical ui-helper-clearfix" );
		$( "#tab_example li" ).removeClass( "ui-corner-top" ).addClass( "ui-corner-left" );
		$( "#tab_example" ).tabs({
		  event: "click" 
		});
		
	})(jQuery)

//	setCookie('miroot7', '', -1);
//	console.log('here');
//	console.log(document.cookie);

	function setCookie(name, value, days) {
	    var d = new Date;
	    d.setTime(d.getTime() + 24*60*60*1000*days);
	    document.cookie = name + "=" + value + ";path=/;expires=" + d.toGMTString();
	}
</script>	
<?php get_footer(); ?>



<?php
	/** Featured Image **/
	add_theme_support('post-thumbnails');

	/** Footer Widgets **/
	register_sidebar( array(
			'name'          => 'Footer 1',
			'id'            => 'footer-1',
			'before_widget' => '',
			'after_widget' => ''
	) );

	register_sidebar( array(
			'name'          => 'Footer 2',
			'id'            => 'footer-2',
			'before_widget' => '',
			'after_widget' => ''
	) );

	register_sidebar( array(
			'name'          => 'Footer 3',
			'id'            => 'footer-3',
			'before_widget' => '',
			'after_widget' => ''
	) );

	register_sidebar( array(
			'name'          => 'Footer 4',
			'id'            => 'footer-4',
			'before_widget' => '',
			'after_widget' => ''
	) );
	
	/** Primary Menu **/
	register_nav_menus(array(
		'primary' => __('Primary Menu', 'My_First_WordPress_Theme'),
		'secondary' => __('Secondary Menu',       'My_First_WordPress_Theme'),
	));

/* 13-12-2017 rinkal Odd Even class  */
// Adds 'odd' and 'even' classes to each post
function wpsd_oddeven_post_class ( $classes ) {
	global $current_class;
	$classes[] = $current_class;
	$current_class = ($current_class == 'odd') ? 'even' : 'odd';
	return $classes;
}
add_filter ('post_class', 'wpsd_oddeven_post_class');
global $current_class;
$current_class = 'odd';



/** Custom Walker Function **/
//add_filter('walker_nav_menu_start_el', 'maiorano_generate_nav_images', 20, 4);
//function maiorano_generate_nav_images($item_output, $item, $depth, $args){
//    if(has_post_thumbnail($item->object_id)){
//        $dom = new DOMDocument(); //DOM Parser because RegEx is a terrible idea
//        $dom->loadHTML($item_output); //Load the markup provided by the original walker
//        $img = $dom->createDocumentFragment(); //Create arbitrary Element
//        $img->appendXML(get_the_post_thumbnail($item->object_id, 'full')); //Apply image data via string
//        $dom->getElementsByTagName('a')->item(0)->appendChild($img); //Append the image to the link
//        $item_output = $dom->saveHTML(); //Replace the original output
//    }
//    return $item_output;
//}

//    class Menu_With_Description extends Walker_Nav_Menu {
//        
//	function start_el( &$output, $item, $depth = 0, $args = Array(), $id = 0) {
//		global $wp_query;
//        $indent;    
//		//$indent = ( $depth ) ? str_repeat( "t", $depth ) : '';
//
//		$class_names = $value = '';
//
//		$classes = empty( $item->classes ) ? array() : (array) $item->classes;
//
//		$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) );
//		$class_names = ' class="' . esc_attr( $class_names ) . '"';
//
//		$output .= $indent . '<li id="menu-item-' . $item->ID . '"' . $value . $class_names . '>';
//
//		$attributes = ! empty( $item->attr_title ) ? ' title="' . esc_attr( $item->attr_title ) . '"' : '';
//		$attributes .= ! empty( $item->target ) ? ' target="' . esc_attr( $item->target ) . '"' : '';
//		$attributes .= ! empty( $item->xfn ) ? ' rel="' . esc_attr( $item->xfn ) . '"' : '';
//		$attributes .= ! empty( $item->url ) ? ' href="' . esc_attr( $item->url ) . '"' : '';
//
//// get user defined attributes for thumbnail images
//		$attr_defaults = array(
//			'class' => 'nav_thumb',
//			'alt'   => esc_attr( $item->attr_title ),
//			'title' => esc_attr( $item->attr_title )
//		);
//		$attr          = isset( $args->thumbnail_attr ) ? $args->thumbnail_attr : '';
//		$attr          = wp_parse_args( $attr, $attr_defaults );
//
//		$item_output = $args->before;
//
//// thumbnail image output
//		$item_output .= ( isset( $args->thumbnail_link ) && $args->thumbnail_link ) ? '<a' . $attributes . '>' : '';
//		$item_output .= apply_filters( 'menu_item_thumbnail', ( isset( $args->thumbnail ) && $args->thumbnail ) ? get_the_post_thumbnail( $item->object_id, ( isset( $args->thumbnail_size ) ) ? $args->thumbnail_size : 'thumbnail', $attr ) : '', $item, $args, $depth );
//		$item_output .= ( isset( $args->thumbnail_link ) && $args->thumbnail_link ) ? '</a>' : '';
//
//// menu link output
//		$item_output .= '<a' . $attributes . '>';
//		$item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
//
//// menu description output based on depth
//		$item_output .= ( $args->desc_depth >= $depth ) ? '<br /><span class="sub">' . $item->description . '</span>' : '';
//
//// close menu link anchor
//		$item_output .= '</a>';
//		$item_output .= $args->after;
//
//		$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
//	}
//}
//
//
//add_filter( 'wp_nav_menu_args', 'my_add_menu_descriptions' );
//function my_add_menu_descriptions( $args ) {
//	$args['walker']         = new Menu_With_Description;
//	$args['desc_depth']     = 0;
//	$args['thumbnail']      = true;
//	$args['thumbnail_link'] = false;
//	$args['thumbnail_size'] = 'nav_thumb';
//	$args['thumbnail_attr'] = array( 'class' => 'nav_thumb my_thumb', 'alt' => 'test', 'title' => 'test' );
//	return $args;
//}
?>






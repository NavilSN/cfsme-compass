<?php get_header();?>
<section id="banner" class="about-us-banner our-testimonials">
        <div class="container">
            <div class="go-down">
                <a class="go_down_anchor" href="#testimonial"><img class="animated fadeIn infinite" src="<?php echo get_template_directory_uri(); ?>/img/down-arrow.png" alt="" class="partner_image"></a>
            </div>
            <div class="banner_text">
                <h1 class="avenir-bold font-38 font-0d75ad text-uppercase">OUR TESTIMONIALS</h1>
                <p class="font-000 avenir-regular font-20">See what client has to say about us!</p>
                <!-- <a href="#" class="banner_find_out_more avenir-regular font-15 font-000">Find Out More</a> -->
            </div>
        </div>
    </section>

    <section id="testimonial" class="reveal_about about_us_top_text our-partners">
        <div class="container">
           <div class="clearfix">
           
            <div class="clearfix wow animated lightSpeedIn">
                <div class="col-md-12">
                    <div class="title_block">
                        <p class="section_subtitle font-18 font-bd8a27 avenir-medium-cond">Client</p>
                        <h2 class="section_title avenir-demi font-38 font-ffffff">
                            <span class="first_letter">T</span>estimonials
                        </h2>
                    </div>

                </div>
            </div>
            
       
          
            
            <div class="testimonial_container">
						<div class="testimonial_block wow animated lightSpeedIn clearfix">
 <?php
                // Start the Loop.
                while ( have_posts() ) : the_post(); ?>
							<div class="author_photo testimonial-singl">
								<img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post_id) ); ?>" alt="">
							</div>
							<div class="author_content">
								<div class="author_name">
									<p class="roboto-regular font-15 font-0d75ad"><?php  echo get_field( 'company_name', get_the_ID() ); ?></p>
								</div>
								<div class="author_testimonial  avenir-regular-italic font-14 font-212121">
									<?php the_content();?>
								</div>
								<div class="author_designation">
									<p class="roboto-regular font-15 font-0d75ad"><?php the_title();?><span class="color-black">  - <?php  echo get_field( 'designation', get_the_ID() ); ?> </span></p>
								</div>
								
								
							
							</div>
							 <?php endwhile;
                        ?>
						</div>
					</div>
					
            
            
           
        </div>
        </div>
    </section>
<?php get_footer();?>

<?php get_header(); ?>
<?php
$s=get_search_query();
$args = array(
's' =>$s
);
// The Query
?>

<section id="banner" class="about-us-banner">
    <div class="container">
        <div class="go-down">
            <a class="go_down_anchor" href="#search">
				<img class="animated fadeIn infinite" src="<?php echo get_template_directory_uri(); ?>/img/down-arrow.png" alt=""></a>
        </div>
        <div class="banner_text">
            <h1 class="avenir-bold font-38 font-0d75ad text-uppercase">
                Search Result
            </h1>
            <!-- <a href="#" class="banner_find_out_more avenir-regular font-15 font-000">Find Out More</a> -->
        </div>
    </div>
</section>

<section id="search" class="reveal_about padding-30">
    <div class="container">
        <?php
		$the_query = new WP_Query( $args );
		if ( $the_query->have_posts() ) {
		_e("<h2 class='section_title section_subtitle font-18 font-bd8a27 avenir-medium-cond'>Search Results for: <span class='query_keyword'>".get_query_var('s')."</span></h2><div class='clearfix'></div>");
		while ( $the_query->have_posts() ) {
		$the_query->the_post();
		?>
		<li class="search_res_list">
			<a href="<?php the_permalink(); ?>">
				<?php the_title(); ?>
			</a>
		</li>
		<?php
		}
		}else{
		?>
		<h2 class="section_title avenir-demi font-38 font-212121">Nothing Found</h2>
		<div class="search_page_alert alert alert-info">
			<p>Sorry, but nothing matched your search criteria. Please try again with some different keywords.</p>
		</div>
		<?php } ?>
    </div>
</section>

<?php get_footer(); ?>
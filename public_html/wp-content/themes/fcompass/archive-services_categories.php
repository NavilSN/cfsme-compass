<?php 

/*
   Template Name: Sservices Inner-list Page
*/   

get_header(); 
?>


  <section id="banner" class="about-us-banner services_inner_list">
    <div class="container">
      <div class="go-down">
        <a class="go_down_anchor" href="#about"><img class="animated fadeIn infinite" src="<?php echo get_template_directory_uri(); ?>/img/down-arrow.png" alt=""></a>
      </div>
      <div class="banner_text">
        <h1 class="avenir-bold font-38 font-0d75ad text-uppercase">Services</h1>
        <p class="font-000 avenir-regular font-20"><?php single_post_title(); ?></p>
        <!-- <a href="#" class="banner_find_out_more avenir-regular font-15 font-000">Find Out More</a> -->
      </div>
    </div>
  </section>
    
    <section id="event-news-list" class="clearfix">
        <div class="container">        
         <div class="row">     
          
            <div class="title_block wow lightSpeedIn">
              <p class="section_subtitle font-18 font-bd8a27 avenir-medium-cond">Services</p>
              <h2 class="section_title avenir-demi font-38 font-ffffff"> <span class="first_letter"> </span> <?php single_post_title(); ?> </h2>
            </div>
            <div class="list-box">               
              <div class="col-md-3 col-sm-3 col-xs-12">
                <div class="img-text">
                  <div class="img-box">
                    <img src="http://192.168.1.33/fcompass/wp-content/uploads/2017/12/1.jpg" alt="">
                  </div>
                  <div class="overlay-text">
                    <p class="font-14 avenir-regular font-fff">Critical Illness</p>
                  </div> 
                    <a href="services_inner.html" class="btn-read font-14 avenir-regular font-fff">Read</a>   
                </div>
              </div>                
                
              <div class="col-md-3">
                <div class="img-text">
                  <div class="img-box">
                    <img src="http://192.168.1.33/fcompass/wp-content/uploads/2017/12/1.jpg" alt="">
                  </div>
                  <div class="overlay-text">
                    <p class="font-14 avenir-regular font-fff">Individual/Group/Employee benefit solutions</p>
                    </div> 
                    
                       <a href="services_inner.html" class="btn-read font-14 avenir-regular font-fff">Read</a>   
                </div>
                </div>
                
               <div class="col-md-3">
                   <div class="img-text">
                    <div class="img-box">
                        <img src="http://192.168.1.33/fcompass/wp-content/uploads/2017/12/1.jpg" alt="">
                    </div>
                    <div class="overlay-text">
                        <p class="font-14 avenir-regular font-fff">Medical Insurance
                        </p>
                        
                        
                    </div> 
                    
                       <a href="services_inner.html" class="btn-read font-14 avenir-regular font-fff">Read</a>   
                </div>
                </div>
                
                
                  <div class="col-md-3">
                   <div class="img-text">
                    <div class="img-box">
                        <img src="http://192.168.1.33/fcompass/wp-content/uploads/2017/12/1.jpg" alt="">
                    </div>
                    <div class="overlay-text">
                        <p class="font-14 avenir-regular font-fff">Accident & Disability Cover
                        </p>
                    
                        
                    </div> 
                    
                       <a href="services_inner.html" class="btn-read font-14 avenir-regular font-fff">Read</a>   
                </div>
                </div>
                
                
                  <div class="col-md-3">
                   <div class="img-text">
                    <div class="img-box">
                        <img src="http://192.168.1.33/fcompass/wp-content/uploads/2017/12/1.jpg" alt="">
                    </div>
                    <div class="overlay-text">
                        <p class="font-14 avenir-regular font-fff">Life Insurance FAQ
                        </p>
                      
                        
                    </div> 
                    
                       <a href="services_inner.html" class="btn-read font-14 avenir-regular font-fff">Read</a>   
                </div>
                </div>
                
           
                
                
                
                
                
                 
                
                 
                
                
              
                
                 
                 
                  </div>
            
            </div> 
        </div>
    </section>


<?php get_footer(); ?>



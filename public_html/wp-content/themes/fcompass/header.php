<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<title>F-Compass</title>
    <link rel="icon" href="<?php echo get_template_directory_uri(); ?>/img/favicon.ico">

	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/style.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/animate.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/owl.carousel.min.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/owl.theme.default.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/font-awesome.min.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/responsive.css">
	<script src="<?php echo get_template_directory_uri(); ?>/js/modernizr.custom.js"></script>
	
		<?php wp_head(); ?>
</head>
<header>
		<nav class="navbar do-main-menu minified" role="navigation">
			<div class="container">
				<!-- Navbar Toggle -->
				<div class="navbar-header">
					<div class="b-container visible-xs visible-sm">
						<div class="b-menu">
							<div class="b-bun b-bun--top"></div>
							<div class="b-bun b-bun--mid"></div>
							<div class="b-bun b-bun--bottom"></div>
						</div>
					</div>

					<!-- Logo START -->
					<div class="logo-box">
						<a href="<?php echo home_url(); ?>" class="textbox-fix">
							<img class="text" src="<?php echo get_template_directory_uri(); ?>/img/logo.png"> </a>
					</div>
					<!-- Logo END -->
				</div>
				<div id="nav-menu" class="navbar-collapse do-menu-wrapper collapse" role="navigation">
		
					<ul id="menu_container" class="nav navbar-nav do-menus">
					
						
					<?php 
//                    wp_nav_menu( array( 'theme_location' => 'primary' ) );
                       
wp_nav_menu( array(
  'menu'        => 'Primary Menu',
    'items_wrap' => '<ul class="nav navbar-nav do-menus" id="menu-primary-navigation">%3$s</ul>',
//    'menu_class' => 'menu-hover company-hover',
  'sub_menu'    => true,
  'show_parent' => true
) );
                    ?>
					</ul>
                
				</div>
				<!-- navbar-collapse end-->

				<!-- SEARCH START -->
				<div class="do-side-menu-opener no-animation searchbox">
				
					<a id="search-icon" class="play-icon popup-with-zoom-anim" href="#0">
						<img class="displayblock" src="<?php echo get_template_directory_uri(); ?>/img/search.png"><img class="displaynone" src="<?php echo get_template_directory_uri(); ?>/img/close.png">
						<!--<img class="displayblock" src="<?php echo get_template_directory_uri(); ?>/img/search.png"><img class="displaynone" src="<?php echo get_template_directory_uri(); ?>/img/close.png">-->
					</a>
                    <div class="search-form displaynone">
                       <div class="slidingDiv" id="slidingDiv">
                        <?php include (TEMPLATEPATH . '/searchform.php'); ?>
                    </div>
                    </div>
				</div>
				<!-- SEARCH END -->
			</div>
		</nav>
		<!-- Navigation Menu end-->
	</header>
<body <?php body_class(); ?>>
	<div id="myNav2" class="overlay2">
		<div class="container">
			<!-- Overlay content -->
			<div class="overlay-content2">
				<div class="btn_container btn-go_back-box">
					<button class="go_back s-font-12 font-fff avenir-regular" id="go_back" onclick="closeNav2()">GO BACK</button>
				</div>
				
				<div class="form_container">
                    <div class="form">
                    <p class="title-get-in-touch avenir-bold font-fff font-20">GET IN TOUCH</p>
				        <?php
						 
						 echo do_shortcode('[contact-form-7 id="78" title="Contact form 1"]');  
						 
						?>
				</div>
			</div>
		</div>
	</div>
	</div>
	<div class="btn_container">
		<button class="s-font-12 btn-quote s-caps" id="apply_btn" onclick="openNav2()">
              GET IN TOUCH
               </button>
	</div>
	

	<div class="b-nav">
	<?php 
//                    wp_nav_menu( array( 'theme_location' => 'primary' ) );
                       
//wp_nav_menu( array(
//  'menu'        => 'Primary Menu',
//    'items_wrap' => '<ul class="menu" id="menu-popup-menu">%3$s</ul>',
////    'menu_class' => 'menu-hover company-hover',
//  'sub_menu'    => true,
//  'show_parent' => true
//) );
                    ?>
	<?php 
        
     wp_nav_menu( array(  'menu'        => 'Primary Menu', 'theme_location' => 'header' ) );
    ?>
    
	</div>


	

	
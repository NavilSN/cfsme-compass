<?php 

/*
   Template Name: Partners page
*/   


get_header(); 
?>
<section id="banner" class="about-us-banner our-partners">
        <div class="container">
            <div class="go-down">
                <a class="go_down_anchor" href="#partner"><img class="animated fadeIn infinite" src="<?php echo get_template_directory_uri(); ?>/img/down-arrow.png" alt="" class="partner_image"></a>
            </div>
            <div class="banner_text">
                <h1 class="avenir-bold font-38 font-0d75ad text-uppercase">OUR PARTNERS</h1>
                <p class="font-000 avenir-regular font-20">Financial Planning Simplified</p>
                <!-- <a href="#" class="banner_find_out_more avenir-regular font-15 font-000">Find Out More</a> -->
            </div>
        </div>
    </section>
<section id="partner" class="reveal_about about_us_top_text our-partners">
        <div class="container">
            <div class="clearfix">
                <div class="col-md-12">
                    <div class="title_block wow lightSpeedIn">
                        <p class="section_subtitle font-18 font-bd8a27 avenir-medium-cond">Our</p>
                        <h2 class="section_title avenir-demi font-38 font-ffffff">
                            <span class="first_letter">P</span>artners
                        </h2>
                    </div>


                                     
                    <?php 
                        $cats = get_categories();
                        foreach ($cats as $cat) {
                         $args = array(
                            'posts_per_page' => 50,
                            'post_type' => 'partner',
                             'category_name' => $cat->slug

                        );
                        $the_query = new WP_Query( $args );
                    ?>
                     <div class="col-md-12">
                        <h2 class="avenir-demi font-22 font-313131"> <?php echo $cat->name;?> </h2>
                    </div>  

                    <?php if ( have_posts() ) : while ( $the_query->have_posts() ) : $the_query->the_post(); ?>                   
                        <div class="col-md-3 col-sm-3 col-xs-12 partner_logo_main">
                            <a class="roboto-bold font-15 font-fff" href="<?php echo get_field( 'partner_website_url', get_the_ID() ); ?>" target="_blank">
                               <div class="partner_logo">
                                <img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post_id) ); ?>" alt="" class="partner_image">
                                <div class="overlay">
                                    <div class="partner_text">
                                        <?php echo the_title(); ?>
                                    </div>
                                </div>
                            </div>
                            </a>
                        </div>
                    <?php endwhile; endif; ?>                      
                    <?php   } ?>

                </div>
            </div>
        </div>
    </section>

<?php get_footer(); ?>

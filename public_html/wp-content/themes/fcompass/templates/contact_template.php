<?php 

/*
   Template Name: Contact page
*/   


get_header(); 
?>
<section id="banner" class="about-us-banner contact-banner">
    <div class="container">
        <div class="go-down">
            <a class="go_down_anchor" href="#contact"><img class="animated fadeIn infinite" src="<?php echo get_template_directory_uri(); ?>/img/down-arrow.png" alt=""></a>
        </div>
        <div class="banner_text">
            <h1 class="avenir-bold font-38 font-0d75ad text-uppercase">connect with us</h1>
            <p class="font-000 avenir-regular font-20">Compass Insurance Brokers LLC</p>
            <!--				<p class="font-000 avenir-regular font-20">Compass Financial Solution</p>-->
            <!-- <a href="#" class="banner_find_out_more avenir-regular font-15 font-000">Find Out More</a> -->
        </div>
    </div>
</section>

<section id="contact" class="reveal_about about_us_top_text">
    <div class="container">
        <div class="clearfix wow lightSpeedIn">
            <div class="col-md-12">
                <div class="title_block">
                    <p class="section_subtitle font-18 font-bd8a27 avenir-medium-cond">Contact</p>
                    <h2 class="section_title avenir-demi font-38 font-212121">
                        <span class="first_letter">G</span>et In Touch
                    </h2>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="careers-form" class="careers-form">
    <div class="container">
        <div class="form-container clearfix">
            <div class="col-md-6">


                    <?php echo do_shortcode('[contact-form-7 id="307" title="Contact Form"]'); ?>

            </div>

            <div class="col-md-6">
                <div class="contact-address">
                    <h3 class="avenir-demi font-0d75ad font-16">COMPASS INSURANCE BROKERS</h3>
                    <p class="avenir-regular font-212121 font-16"><?php echo get_field( 'contact_address', get_the_ID() ); ?></p>
                    <div class="contact-number">
                        <span class="avenir-regular font-212121 font-16"><i class="fa fa-phone"></i> <?php echo get_field( 'contact_number', get_the_ID() ); ?></span>
                        <span class="avenir-regular font-212121 font-16"><i class="fa fa-print"></i> <?php echo get_field( 'contact_fax', get_the_ID() ); ?></span>
                        <span class="avenir-regular font-212121 font-16"><i class="fa fa-envelope"></i> <?php echo get_field( 'contact_mail', get_the_ID() ); ?></span>
                        <?php the_post(); ?><?php the_content(); ?> 
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="map">
        <?php echo get_field( 'google_map', get_the_ID() ); ?>
    </div>
</section>

<?php get_footer(); ?>

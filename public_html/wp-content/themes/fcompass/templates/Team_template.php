<?php 

/*
   Template Name: Team page
*/   


get_header(); 
?>
<section id="banner" class="about-us-banner team-banner">
    <div class="container">
        <div class="go-down">
            <a class="go_down_anchor" href="#about"><img class="animated fadeIn infinite" src="<?php echo get_template_directory_uri(); ?>/img/down-arrow.png" alt=""></a>
        </div>
        <div class="banner_text">
            <h1 class="avenir-bold font-38 font-0d75ad text-uppercase">THE COMPASS TEAM</h1>
            <p class="font-000 avenir-regular font-20">We possess a strong and worthy leadership</p>
            <!-- <a href="#" class="banner_find_out_more avenir-regular font-15 font-000">Find Out More</a> -->
        </div>
    </div>
</section>

<section id="about" class="reveal_about about_us_top_text our-team">
    <div class="container">
        <?php the_post(); ?>
        <?php the_content() ?>
    </div>
</section>



<section id="#" class="team-details">
    <div class="container">
        <?php
 
$args = array(
    'post_type' => 'team',
    'orderby'=>'menu_order',
    'order' => 'ASC',
);
 
// Custom query.
$query = new WP_Query( $args );
 
// Check that we have query results.
if ( $query->have_posts() ) {
 
  
 
        // Start looping over the query results.
       while ( $query->have_posts() ) : $query->the_post();  
            ?>
            <div <?php post_class(); ?>>
                <div class="col-md-6 col-sm-12">

                    <div class="img-box wow flipInX center">
                        <?php the_post_thumbnail(); ?>
                        <!--							<img src="<?php //echo get_template_directory_uri(); ?>/Nihalani.png" title="Nihalani">											-->
                    </div>

                </div>
                <div class="col-md-6 col-sm-12">

                    <div class="content-box">
                        <h3 class="margin30top font-20 font-343434 text-uppercase letter4spacing avenir-demi">
                            <?php echo the_title(); ?>
                            <span class="font-12 font-343434 text-uppercase letter4spacing line-h-20 avenir-regular">
								    <?php  echo get_field( 'team_member_designation', get_the_ID() ); ?>
								</span>
                        </h3>


                        <div class="more">		
                        <p class="font-343434 font-16 line-h-21 avenir-regular">
								<?php echo the_content(); ?>
							</p>							
                            <div id="expand_<?php echo get_the_ID(); ?>" class="collapse">
                                <p class="font-343434 font-16 line-h-21 avenir-regular">
                                    <?php  echo get_field( 'team_member_expandable_content', get_the_ID() ); ?>
                                </p>
                            </div>
							<button class="btn-expand" data-toggle="collapse" data-target="#expand_<?php echo get_the_ID(); ?>">Read More +</button>
						</div>

                    </div>

                </div>
            </div>
            <?php     
        endwhile;
		}
		
 

 
// Restore original post data.
wp_reset_postdata();
 
?>
    </div>
</section>
 <script>
        $('.btn-expand').click(function() { //you can give id or class name here for $('button')
            $(this).text(function(i, old) {
                return old == 'Read More +' ? 'Close' : 'Read More +';
            });
        });
    </script>


<?php get_footer(); ?>

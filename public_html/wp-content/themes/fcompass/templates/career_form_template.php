<?php 

/*
   Template Name: Career Form page
*/   

get_header(); 
?>

<?php $position_id = $_POST['position_id'] ;
$position_title = get_the_title($position_id);?>


<section id="banner" class="about-us-banner career-banner">
        <div class="container">
            <div class="go-down">
                <a class="go_down_anchor" href="#career"><img class="animated fadeIn infinite" src="<?php echo get_template_directory_uri(); ?>/img/down-arrow.png" alt=""></a>
            </div>
            <div class="banner_text">
                <h1 class="avenir-bold font-38 font-0d75ad text-uppercase">Career Opportunities</h1>
                <p class="font-000 avenir-regular font-20"> Financial Planning Simplified</p>
            </div>
        </div>
    </section>
   
    <section id="careers-form"  class="careers-form">
        <div class="container">     
            <div class="title_block wow lightSpeedIn">
                <p class="section_subtitle font-18 font-bd8a27 avenir-medium-cond">Careers</p>
                <h2 class="section_title avenir-demi font-38 font-ffffff">
                    <span class="first_letter">B</span>e a part of Compass Team
                </h2>
            </div>
            
            <div class="center-para">
                <p class="avenir-demi font-15 font-212121"> If you think you can comprehend the huge arena of Insurance Services and Financial Planning, and you really wish to get the international exposure, join us! We – the Team Compass, are ever learning, ever evolving with the world. That is how we get to be the best. Fill in your details and we shall get back to you with more information. </p>
            </div>
          
          <div class="form-container">  

          	<?php echo do_shortcode( '[contact-form-7 id="306" title="career form"]' ); ?>
          

          </div>          
           
       
        </div>
    </section>
    
    

<?php get_footer(); ?>
<!-- to set position value in form -->
<script type="text/javascript">
jQuery(document).ready(function(){
  <?php if($position_title!=""){ ?>
        $("#position").val("<?php echo $position_title; ?>");
  <?php } ?>

  });
</script>



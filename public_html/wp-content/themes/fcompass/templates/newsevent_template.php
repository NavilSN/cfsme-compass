<?php 

/*
   Template Name: News-Event page
*/   


get_header(); 
?>
<section id="banner" class="about-us-banner news-event-banner">
		<div class="container">
			<div class="go-down">
				<a class="go_down_anchor" href="#event-news"><img class="animated fadeIn infinite" src="<?php echo get_template_directory_uri(); ?>/img/down-arrow.png" alt=""></a>
			</div>
			<div class="banner_text">
				<h1 class="avenir-bold font-38 font-0d75ad text-uppercase">Latest news</h1>
				<p class="font-000 avenir-regular font-20">How to Choose a Financial Planner?</p>
				<!-- <a href="#" class="banner_find_out_more avenir-regular font-15 font-000">Find Out More</a> -->
			</div>
		</div>
	</section>
<section id="event-news" class="clearfix">
        <div class="container">
         <div class="clearfix">
          <div class="title_block wow lightSpeedIn">
                        <p class="section_subtitle font-18 font-bd8a27 avenir-medium-cond">UPDATES</p>
                        <h2 class="section_title avenir-demi font-38 font-ffffff">
                            <span class="first_letter">N</span>ews & Events
                        </h2>
                    </div>
        
        
        
       
             <div class="news-box">
<?php  
                $args = array(
                    'post_type' => 'post',
                    'numberposts' => '3',
                     'posts_per_page' => 3,
                    'orderby' => 'post_date',
                    'post_status' => 'draft, publish, future, pending, private',
	               'suppress_filters' => true
                );
                $count = 1;

                $post_query = new WP_Query($args);
                if($post_query->have_posts() ) {
                  while($post_query->have_posts() ) {
                    $post_query->the_post();
            ?>
            <div class="col-md-4 col-xs-12 col-sm-6">
                   <div class="black-line<?php echo $count;?>"></div>
                    <div class="content-box">
                      
                       <p class="font-25 avenir-regular font-000"> <?php the_title();?></p>
                       <a href="<?php the_permalink(); ?>" class="font-bd8a27 btn-view">View</a>
                    </div>
                    <div class="time-date-box">
                        <?php $date = new DateTime(get_field( 'news-events_date', get_the_ID() )); ?> 
                        <p class="font-15 avenir-regular"> <?php echo $date->format('j M Y'); ?></p>
                        <p class="font-15 avenir-regular"><?php echo get_field( 'news-events_time', get_the_ID() ); ?></p>
                    </div>
                 </div>
            
              <div class="col-md-4 col-xs-12 col-sm-6">                    
                    <div class="img-box">
                        <?php the_post_thumbnail(); ?>
                    </div>
                </div>
                <?php
                    $count++;
									  }
									}
								?>
                <?php wp_reset_query(); ?>
                </div>
            <div class="button">
                <a href="<?php the_permalink(); ?>news-events-all/" class="view-button">View All</a>
            </div>
            
           
            </div> 
        </div>
    </section>

<?php get_footer(); ?>

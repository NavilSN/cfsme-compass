<?php 

/*
   Template Name: Director's Mesage page
*/   


get_header(); 
?>

<section id="banner" class="about-us-banner news-event-banner">
    <div class="container">
        <div class="go-down">
            <a class="go_down_anchor" href="#about"><img class="animated fadeIn infinite" src="<?php echo get_template_directory_uri(); ?>/img/down-arrow.png" alt=""></a>
        </div>
        <div class="banner_text">
            <h1 class="avenir-bold font-38 font-0d75ad text-uppercase">Director's Message</h1>
        </div>
    </div>
</section>
<section id="newsevent-detail" class="">
    <div class="container">

        <div class="clearfix">
            <div class="title_block wow lightSpeedIn">
                <h2 class="section_title avenir-demi font-38 font-ffffff">
                    <span class="first_letter">D</span>irector's Message
                </h2>
            </div>

            <div class="detail-box">
                <div class="col-md-12 col-sm-12">
                    <?php
                while ( have_posts() ) : the_post(); ?>
                        <div class="img-box text-center">
                            <?php the_post_thumbnail(); ?>
                        </div>
                        <!-- <div class="detail">
                            <p class="font-22 avenir-demi font-000">
                                <?php the_title(); ?>
                            </p>
                            <p class="font-18 avenir-regular font-000">  <?php  echo get_field( 'management_message_designation', get_the_ID() ); ?></p>
                        </div> -->
                        <div class="discription-para">
                            <p class="font-16 avenir-demi font-000">
                                <?php the_content(); ?>
                            </p>
                        </div>
                        <?php endwhile;
                        ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php get_footer();?>

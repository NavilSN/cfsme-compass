<?php 

/*
   Template Name: TheCompany page
*/   


get_header(); 
?>
<section id="banner" class="about-us-banner">
    <div class="container">
        <div class="go-down">
            <a class="go_down_anchor" href="#about">
				<img class="animated fadeIn infinite" src="<?php echo get_template_directory_uri(); ?>/img/down-arrow.png" alt=""></a>
        </div>
        <div class="banner_text">
            <h1 class="avenir-bold font-38 font-0d75ad text-uppercase">
                <?php the_title() ?>
            </h1>
            <p class="font-000 avenir-regular font-20">Compass Insurance Brokers LLC</p>
            <!-- <a href="#" class="banner_find_out_more avenir-regular font-15 font-000">Find Out More</a> -->
        </div>
    </div>
</section>

<section id="about" class="reveal_about about_us_top_text">
    <div class="container">
        <?php the_post(); ?>
        <?php the_content() ?>
    </div>
</section>

<section id="professional" class="about-tab-details">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <script type="text/javascript">
                    (function($) {
                        fakewaffle.responsiveTabs(['xs', 'sm']);
                    })(jQuery);

                </script>

                <div class="tabbable-panel">
                    <div class="tabbable-line">
                        <ul class="nav nav-tabs responsive">
                            <?php
						 	$args = array(
			                    'post_type' => 'company_tab',
			                    'orderby'          => 'title',
								'order'            => 'ASC',
			                );
		                    $the_query = new WP_Query( $args );
		                    $count = 1;              
		                    while ( $the_query->have_posts() ) : $the_query->the_post();  
		                ?>
                                <li class="<?php if($count==1) echo " active "; ?>">
                                    <a href="#tab_default_<?php echo $count?>" class="avenir-demi font-22 font-fff text-uppercase" data-toggle="tab">
                                        <?php echo the_title();  ?>
                                    </a>
                                </li>
                                <?php $count= $count+1;  ?>
                                <?php endwhile;?>
                        </ul>
                        <?php wp_reset_postdata(); ?>


                        <div class="tab-content responsive">
                            <?php
						 	$args = array(
			                    'post_type' => 'company_tab',
			                    'orderby'          => 'title',
								'order'            => 'ASC',
			                );
		                    $the_query = new WP_Query( $args );
		                    $count_desc = 1;              
		                    while ( $the_query->have_posts() ) : $the_query->the_post();  
		                ?>

                                <div class="tab-pane <?php if($count_desc==1) echo " active "; ?>" id="tab_default_<?php echo $count_desc;?>">
                                    <p class="font-16 font-fff avenir-regular line-h-24">
                                        <?php echo get_the_content(); ?>
                                    </p>
                                    <div id="expand_<?php echo $count_desc; ?>" class="collapse">
                                        <p class="font-16 font-fff avenir-regular line-h-24">
                                            <?php echo get_field( 'expandable_content', get_the_ID() )  ?>
                                        </p>
                                    </div>
                                    <button class="btn-expand more-white" data-toggle="collapse" data-target="#expand_<?php echo $count_desc; ?>">Read More +</i></button>
                                </div>
                                <?php $count_desc++;  ?>
                                <?php endwhile;?>
                                <?php wp_reset_postdata(); ?>
                        </div>
                    </div>
                </div>
            </div>
</section>

<section id="corporate-det" class="corporate-det">
    <div class="container">
        <?php
                    $the_query = new WP_Query( 'pagename=the-company' );
                    while ( $the_query->have_posts() ) : $the_query->the_post();
                ?>
            <div class="col-md-6 col-sm-12 pull-left">
                <div class="row">
                    <div class="img-box wow flipInX center">
                        <img src="<?php print_r( get_field( 'about_company_corporate_responsibility_image', get_the_ID() )['url']); ?>" title="corporates">
                        <a href="javascript:void(0)" class="corporate-link">corporate social responsibility</a>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-12 pull-left">
                <div class="row">
                    <div class="content-box">
                        <span class="center-border top20"></span>
                        <p class="font-343434 font-16 line-h-24">
                            <?php echo get_field( 'about_company_corporate_responsibility_content', get_the_ID() ); ?>
                        </p>
                        <span class="center-border buttom16"></span>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-12 pull-right">
                <div class="row">
                    <div class="img-box wow flipInX center">
                        <img src="<?php print_r( get_field( 'about_company_mart_member_image', get_the_ID() )['url']); ?>" title="corporates">
                        <a href="javascript:void(0)" class="corporate-link">An MDRT Member</a>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-12 pull-left">
                <div class="row">
                    <div class="content-box">
                        <span class="center-border top20"></span>
                        <p class="font-343434 font-16 line-h-24">
                            <?php echo get_field( 'about_company_mart_member_content', get_the_ID() ); ?>
                        </p>
                        <span class="center-border buttom16"></span>
                    </div>
                </div>
            </div>


            <?php 
                    endwhile;
                    wp_reset_postdata();
                ?>
    </div>
</section>


<?php get_footer(); ?>

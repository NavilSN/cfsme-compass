<?php 

/*
   Template Name: Career page
*/   


get_header(); 
?>
<section id="banner" class="about-us-banner career-banner">
        <div class="container">
            <div class="go-down">
                <a class="go_down_anchor" href="#career"><img class="animated fadeIn infinite" src="<?php echo get_template_directory_uri(); ?>/img/down-arrow.png" alt=""></a>
            </div>
            <div class="banner_text">
                <h1 class="avenir-bold font-38 font-0d75ad text-uppercase">Career Opportunities</h1>
                <p class="font-000 avenir-regular font-20">Financial Planning Simplified</p>
            </div>
        </div>
    </section>
<section id="career" class="reveal_about about_us_top_text">
        <div class="container">
            <div class="clearfix wow lightSpeedIn">
                <div class="col-md-12">
                    <div class="title_block">
                        <p class="section_subtitle font-18 font-bd8a27 avenir-medium-cond">Careers</p>
                        <h2 class="section_title avenir-demi font-38 font-212121">
                            <span class="first_letter">V</span>acancies
                        </h2>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="corporate-det" class="career-section">
        <div class="container">
           <?php
 
$args = array(
    'post_type' => 'careers',
    'order' => 'DESC'
);
 
// Custom query.
$query = new WP_Query( $args );
 
// Check that we have query results.
if ( $query->have_posts() ) {
 
  
 
        // Start looping over the query results.
        while ( $query->have_posts() ) {
 
            $query->the_post();
 
            ?>
           <h3 class="font-27 font-212121 avenir-demi"><?php the_title(); ?></h3>
            <h4  class="font-20 font-212121 avenir-demi">Department :  <?php echo get_field( 'job_department', get_the_ID() ); ?></h4>
            <span class="line-bottom"></span>
            <div class="career-role-list">
                <h4 class="font-20 font-212121 avenir-demi">Responsibilities </h4>
                <div class="role-list">
                    <p><?php echo get_field( 'job_responsibility', get_the_ID() ); ?></p>
                </div>
           
                <h4 class="font-20 font-212121 avenir-demi">Skills & Qualifications </h4>
                <div class="role-list">
                   <p> <?php echo get_field( 'job_skill_qualification', get_the_ID() ); ?></p>
                </div>
                <form action="<?php the_permalink(); ?>../../apply-for/" method="POST"> 
                    <input type="hidden" name="position_id" value="<?php echo get_the_ID()?>">
                    <input type="submit" name="apply" value="APPLY" class="btn btn-pagination-btn avenir-regular font-14 font-0d75ad">
                       
                </form>
                 
            </div>
            <?php
     
        }
 
   
 
}
 
// Restore original post data.
wp_reset_postdata();
 
?>
        </div>
         
    </section>

<?php get_footer(); ?>

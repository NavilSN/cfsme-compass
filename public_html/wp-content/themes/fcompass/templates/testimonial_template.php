<?php 

/*
   Template Name: Testimonial page
*/   


get_header(); 
?>
<section id="banner" class="about-us-banner our-testimonials">
        <div class="container">
            <div class="go-down">
                <a class="go_down_anchor" href="#testimonial"><img class="animated fadeIn infinite" src="<?php echo get_template_directory_uri(); ?>/img/down-arrow.png" alt="" class="partner_image"></a>
            </div>
            <div class="banner_text">
                <h1 class="avenir-bold font-38 font-0d75ad text-uppercase">OUR TESTIMONIALS</h1>
                <p class="font-000 avenir-regular font-20">See what client has to say about us!</p>
                <!-- <a href="#" class="banner_find_out_more avenir-regular font-15 font-000">Find Out More</a> -->
            </div>
        </div>
    </section>

    <section id="testimonial" class="reveal_about about_us_top_text our-partners">
        <div class="container">
           <div class="clearfix">
            <div class="clearfix wow animated lightSpeedIn">
                <div class="col-md-12">
                    <div class="title_block">
                        <p class="section_subtitle font-18 font-bd8a27 avenir-medium-cond">Client</p>
                        <h2 class="section_title avenir-demi font-38 font-ffffff">
                            <span class="first_letter">T</span>estimonials
                        </h2>
                    </div>

                </div>
            </div>
            <div class="clearfix">
                <div class="col-md-12 col-sm-12">

<?php $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

 $args = array('post_type' => 'testimonials',
                'order' => 'DESC',
                'posts_per_page' => 9, 
                'paged' => $paged,
              );
  $count = 0;
  // Custom query.
  $query = new WP_Query( $args );
 
  // Check that we have query results.
  if ( $query->have_posts() ) {     ?>
                <div class="col-md-12 col-sm-12">
                    <div class="grid-masonry">
                        <ul class="grid effect-2 testimonial-masonry" id="grid" >
                        <?php                        


        // Start looping over the query results.
        while ( $query->have_posts() ) {
 
            $query->the_post();
?>
                            <li class="col-md-4" >
                           <a href="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post_id) ); ?>" class="popup_link">
                           <div class="grid-item grid-item--height<?php if($count%2 == 0) { echo "2"; }else{ echo "3"; } ?>  text-center">
<!--                            <div class="gallery_main_block">-->
                            <img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post_id) ); ?>" alt="">
                            <h3 class="testimonial_name avenir-demi font-18 font-212121"><?php the_title();?></h3>
                             <h3 class="testimonial_name avenir-demi font-18 font-212121"><?php  echo get_field( 'company_name', get_the_ID() ); ?></h3>
                             <h3 class="testimonial_designation avenir-regular font-16 font-212121"><?php  echo get_field( 'designation', get_the_ID() ); ?></h3>
                            <span class="line"></span>
                            <p class="testimonial_text avenir-regular font-18 font-212121"><?php 
                            $content = get_the_content();
                            $content = strip_tags($content);        
                               if($count%2 == 0) { 
                                echo substr($content, 0, 100);
                                 }else{ 
                                  echo substr($content, 0, 150);
                                   } ?>                            </p>
                            <a href="<?php the_permalink(); ?>" class="testimonial-link-target"></a>

                                     
                                    </div>
                                </a>
                            </li>
                            <?php 

                             $count++;

                        }
 

// Restore original post data.
?>
                        </ul>

                    </div>

                </div> <!-- html -->
<?php wp_reset_postdata();
  // next_posts_link() usage with max_num_pages
   ?>
   <div class="pagination-btn text-center">
   <?php echo next_posts_link( 'Next', $query->max_num_pages ); 
   echo previous_posts_link( 'Prev' );
   ?>
                           <!-- <a href="#" class="btn btn-pagination-btn avenir-regular font-14 font-0d75ad">
                              Prev 
                           </a> -->
                           <!-- <a href="#" class="btn btn-pagination-btn avenir-regular font-14 font-0d75ad">
                              Next 
                           </a> -->
                           
                       </div>
  <?php 



}else{  ?>
<p><?php _e( 'Sorry, no testimonial found.' ); ?></p>
<?php }; ?>


                 
                   
                </div>

            </div>
        </div>
        </div>
    </section>

<?php get_footer(); ?>

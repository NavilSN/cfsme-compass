<?php 

/*
   Template Name: News and Event List page
*/   


get_header(); 
?>
<section id="banner" class="about-us-banner news-event-banner">
		<div class="container">
			<div class="go-down">
				<a class="go_down_anchor" href="#about"><img class="animated fadeIn infinite" src="<?php echo get_template_directory_uri(); ?>/img/down-arrow.png" alt=""></a>
			</div>
			<div class="banner_text">
				<h1 class="avenir-bold font-38 font-0d75ad text-uppercase">Latest news</h1>
				<p class="font-000 avenir-regular font-20">How to Choose a Financial Planner?</p>
				<!-- <a href="#" class="banner_find_out_more avenir-regular font-15 font-000">Find Out More</a> -->
			</div>
		</div>
	</section>
 <section id="event-news-list" class="clearfix">
        <div class="container">
        
         <div class="row">
        
          
          <div class="title_block wow lightSpeedIn">
                        <p class="section_subtitle font-18 font-bd8a27 avenir-medium-cond">UPDATES</p>
                        <h2 class="section_title avenir-demi font-38 font-ffffff">
                            <span class="first_letter">N</span>ews & Events
                        </h2>
                    </div>      
       
            <div class="list-box clearfix">
              <?php
              $args = array(
                 'post_type' => 'post',
                  'orderby' => 'post_date',
                  'post_status' => 'draft, publish, future, pending, private',
                 'suppress_filters' => true
              );
              $the_query = new WP_Query( $args );
              $count = 1;              
              while ( $the_query->have_posts() ) : $the_query->the_post();  
              ?>
                  <div class="col-md-3 col-xs-12 col-sm-6">
                    <div class="img-text">
                      <div class="img-box">                           
                        <?php the_post_thumbnail(); ?>
                      </div>
                      <div class="overlay-text">
                          <p class="font-14 avenir-regular font-fff"><?php the_title();   ?>
                          </p>
                           <?php $date = new DateTime(get_field( 'news-events_date', get_the_ID() )); ?> 
                          <p class="font-12 avenir-regular font-fff"><?php echo $date->format('j - M - Y'); ?></p>
                      </div> 
                         <a href="<?php the_permalink(); ?>" class="btn-read font-14 avenir-regular font-fff">Read</a>   
                    </div>
                  </div>
                <?php endwhile;?>
                <?php wp_reset_postdata(); ?>
        
                 
                  </div>
            
            </div> 
        </div>
    </section>

<?php get_footer(); ?>

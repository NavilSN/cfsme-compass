<?php 

/*
   Template Name: Home page
*/   


get_header(); ?>
<section id="banner">
	<?php echo do_shortcode("[metaslider id=129]"); ?>
<!--		<div class="container">-->
		
			<div class="go-down">
				<a class="go_down_anchor" href="#about"><img class="animated fadeIn infinite" src="<?php echo get_template_directory_uri(); ?>/img/down-arrow.png" alt=""></a>
			</div>
<!--
			<div class="banner_text">
				<h1 class="avenir-bold font-38 font-0d75ad text-uppercase">Life Insurance</h1>
				<p class="font-000 avenir-regular font-20">No one can predict the future-which makes it all the more important to be prepared...</p>
				<a href="#" class="banner_find_out_more avenir-regular font-15 font-000">Find Out More</a>
			</div>
-->
<!--		</div>-->
	</section>
<section id="about" class="reveal_about">
    <div class="container">
        <div class="shape3-container">
            <img src="<?php echo get_template_directory_uri(); ?>/img/section-inclined-img.png" class="shape3" style="display:none;" alt="">
        </div>
        <div class="section_left">
            <!--<img src="img/about-company.png" alt="">-->
            <img src="<?php print_r( get_field( 'home_company_image1', get_the_ID() )['url'] ); ?>" alt="" data-wow-offset="10" class="shape1 wow fadeInDown">
            <img src="<?php print_r( get_field( 'home_company_image2', get_the_ID() )['url'] ); ?>" alt="" data-wow-offset="10" class="shape2 wow fadeInUp">
        </div>
        <div class="section_right col-sm-6">
            <div class="title_block">
                <p class="section_subtitle font-18 font-343434 avenir-medium-cond">About Company</p>
                <h2 class="section_title avenir-demi font-30 font-343434">
                    <span class="first_letter">C</span>ompass Financial Solutions
                </h2>
            </div>

            <p class="about_tagline bg-text bg-0d75ad font-20 font-fff avenir-demi-italic">
                <?php  echo get_field( 'home_company_subtitle', get_the_ID() ); ?>
            </p>

            <p class="about_content font-737373 font-15 avenir-regular">
                <?php echo wp_trim_words(  get_field( 'home_company_subcontent', get_the_ID() ), 50, '...' );   ?>

            </p>

            <a href="<?php echo site_url(); ?>/the-company/" class="about_read_more font-0d75ad avenir-regular ">Read More</a>
        </div>
    </div>
</section>

<section id="professional">
    <div class="container">
       <div class="service_container title_block">
        <div class="title_block">
            <p class="section_subtitle avenir-medium-cond font-18">Professional</p>
            <h2 class="section_title avenir-demi font-30 font-fff">
                <span class="first_letter">S</span>ervices
            </h2>
        </div>
        </div>
       <div class="main-service">
        
        <!-- Service Block -->
        <?php
          $args = array(
              'hide_empty' => false
          );
          $category = get_terms( 'professional_service', $args ); // get service categories terms
          $service_count = 1 ;
          foreach ($category as $catVal) { 
                    
            $cat_id = $catVal->term_id;
            $thumbnail = get_field('service-icon-white', $catVal->taxonomy . '_' . $catVal->term_id);
             $thumbnail_icon = get_field('service-icon', $catVal->taxonomy . '_' . $catVal->term_id);
             if($catVal->term_id == 10 || $catVal->term_id== 12){
                $cate_link = site_url().'/professional-services';
             }else{
                $cate_link = get_category_link($catVal->term_id); 
             }
           ?>

            <div class="col-sm-3 service_container">
                <div class="wrap">
                    <div class="box">
                        <div class="box__right">
                            <img src="<?php echo $thumbnail; ?>" alt="service-icon">
                            <p class="service_title roboto-bold font-18 font-fff">
                                <?php echo $catVal->name ; ?> 
                            </p>
                            <a href="<?php echo $cate_link ?>">Read More</a>
                        </div>
                        <div class="box__left">
                            <img src="<?php echo $thumbnail; ?>" alt="service-icon">
                            <p class="service_title roboto-bold font-18 font-fff">
                                <?php echo $catVal->name ; ?> 
                            </p>
                            <a href="<?php echo $cate_link; ?>">Read More</a>
                        </div>
                        <div class="box__top">
                            <img src="<?php echo $thumbnail; ?>" alt="service-icon">
                            <p class="service_title roboto-bold font-18 font-fff">
                                <?php echo $catVal->name ; ?> 
                            </p>
                            <a href="<?php echo $cate_link; ?>">Read More</a>
                        </div>
                        <div class="box__bottom">
                            <img src="<?php echo $thumbnail;  ?>" alt="service-icon">
                            <p class="service_title roboto-bold font-18 font-fff">
                                <?php echo $catVal->name ; ?> 
                            </p>
                            <a href="<?php echo $cate_link; ?>">Read More</a>
                        </div>
                        <div class="box__center">
                            <div class="inner">
                                <img src="<?php echo $thumbnail_icon; ?>" alt="service-icon-white">
                                <p class="service_title roboto-bold font-18 font-fff">
                                    <?php echo $catVal->name ; ?> 
                                </p>
                                <p class="service_title roboto-regular font-12 font-fff">
                                    <?php echo wp_trim_words( $catVal->description, 7, '...' );?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

             <?php $service_count++; 
               }?>
    </div>
    </div>
</section>

<section id="testimonials">
    <div class="container">
        <div class="title_block">
            <p class="section_subtitle font-18 font-bd8a27 avenir-medium-cond">Client's</p>
            <h2 class="section_title avenir-demi font-30 font-343434">
                <span class="first_letter">T</span>estimonials
            </h2>
        </div>

        <div class="row testimonial_row">
            <div class="owl-carousel">

                <?php
 
$args = array(
    'post_type' => 'testimonials'
);
 
// Custom query.
$query = new WP_Query( $args );
 
// Check that we have query results.
if ( $query->have_posts() ) {
 
  
 
        // Start looping over the query results.
        while ( $query->have_posts() ) {
 
            $query->the_post();
 
            ?>

                    <div class="testimonial_container">
                        <div class="testimonial_block clearfix">
                            <div class="author_photo">
                                <?php the_post_thumbnail(); ?>
                            </div>
                            <div class="author_content">
                                <div class="author_name">
                                    <p class="roboto-regular font-15 font-0d75ad">
                                        <?php the_title(); ?>
                                    </p>
                                </div>
                                <div class="author_testimonial  avenir-regular-italic font-15 font-212121">
                                    <?php echo wp_trim_words( get_the_content(), 7, '...' ); ?>
                                </div>
                            </div>
                        </div>
                    </div>






                    <?php
     
        }
 
   
 
}
 
// Restore original post data.
wp_reset_postdata();
 
?>
            </div>
        </div>
    </div>
</section>


<section class="latest_news">
    <div class="left">
        <div class="title_block">
            <p class="section_subtitle font-18 font-bd8a27 avenir-medium-cond">Latest</p>
            <h2 class="section_title avenir-demi font-30 font-343434">
                <span class="first_letter">N</span>ews
            </h2>
        </div>
        <div class="latest_news_container">
            <?php  
                $args = array(
                    'post_type' => 'post',
                    'numberposts' => '3',
                     'posts_per_page' => 3,
                    'orderby' => 'post_date',
                    'post_status' => 'draft, publish, future, pending, private',
	               'suppress_filters' => true
                );

                $post_query = new WP_Query($args);
                if($post_query->have_posts() ) {
                  while($post_query->have_posts() ) {
                    $post_query->the_post();
            ?>
            <div class="news-block">
                <div class="news_image">
                    <?php the_post_thumbnail(); ?>
                </div>

                <div class="news_content">
                    <p class="news_title text-uppercase font-15 avenir-demi font-212121">
                        <?php the_title(); ?>
                    </p>
                    <p class="news_date text-uppercase font-14 avenir-regular font-212121">
                        <?php the_time('M d Y'); ?>
                    </p>
                    <p class="news_content font-14 avenir-regular font-212121">
                        <?php echo wp_trim_words( get_the_content(), 7, '...' );  ?>
                    </p>
                    <a href="<?php the_permalink(); ?>" class="news_read_more avenir-regular font-14 font-0d75ad">Read More</a>
                </div>
            </div>
            <?php
									  }
									}

								?>
                <?php wp_reset_query(); ?>
        </div>

    </div>
    <div class="right">
        <div class="from_the_management_section">
            <div class="title_block">
                <p class="section_subtitle font-18 font-fff avenir-medium-cond">From the</p>
                <h2 class="section_title avenir-demi font-30 font-343434">
                    <span class="first_letter">M</span>anagement
                </h2>
            </div>

            <div class="from_management_content">
             <?php
                    $the_query = new WP_Query( 'pagename=director-message' );
                    while ( $the_query->have_posts() ) : $the_query->the_post();
                ?>

                <div class="mgmt_img">
                    <img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id($post_id) ); ?>" alt="">

                </div>

                <div class="mgmt_content">
               
                    <div class="msg_title">
                        <p class="avenir-demi text-uppercase font-14 font-fff">
                             <?php echo get_field( 'management_message_title', get_the_ID() ); ?>
                        </p>
                    </div>
                    <div class="msg_designation">
                        <p class="avenir-regular text-uppercase font-14 font-fff">
                            <?php  echo get_field( 'management_message_designation', get_the_ID() ); ?>
                        </p>
                    </div>

                    <div class="msg_content">
                        <p class=" avenir-regular font-14 font-fff">
                            <?php echo wp_trim_words( get_the_content(), 37, '...' );?>
                        </p>
                    </div>

                    <a href="<?php the_permalink(); ?>director-message/" class="msg_read_more avenir-regular font-14 font-fff">Read More</a>
                </div>
            </div>
             <?php 
                    endwhile;
                    wp_reset_postdata();
                ?>
        </div>
    </div>
</section>

<!--
<section id="gallery">
    <div class="container">
      <div class="">
       <?php// echo do_shortcode('[aigpl-gallery-slider id="155" grid="6" gallery_height="158" dots="false" arrows="false" slidestoscroll="1" slidestoshow="6"]'); ?>
        </div>
       
    </div>
</section>
-->
<?php get_footer(); ?>

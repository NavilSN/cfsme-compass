<?php 

/*
   Template Name: Gallery page
*/   


get_header(); 
?>
<section id="banner" class="about-us-banner our-gallery">
        <div class="container">
            <div class="go-down">
                <a class="go_down_anchor" href="#gallery-main"><img class="animated fadeIn infinite" src="<?php echo get_template_directory_uri(); ?>/img/down-arrow.png" alt="" class="partner_image"></a>
            </div>
            <div class="banner_text">
                <h1 class="avenir-bold font-38 font-0d75ad text-uppercase">PICTURE GALLEY</h1>
            </div>
        </div>
    </section>
<section id="gallery-main" class="reveal_about about_us_top_text our-partners">
        <div class="container">
<!--
            <div class="shape3-container">
                <img src="img/section-inclined-img.png" class="shape3" style="display:none;" alt="" class="partner_image">
            </div>
-->
            <div class="clearfix wow animated lightSpeedIn">
                <div class="col-md-12">
                    <div class="title_block">
                        <p class="section_subtitle font-18 font-bd8a27 avenir-medium-cond">Our</p>
                        <h2 class="section_title avenir-demi font-38 font-ffffff">
                            <span class="first_letter">G</span>allery
                        </h2>
                    </div>

                </div>
            </div>
            <div class="clearfix">
                <div class="col-md-12 col-sm-12">
                    <div class="grid-masonry">
<!--                        <ul class="grid effect-2" id="grid" style="position: relative; height: 9750.47px; perspective-origin: 50% 3718.5px;">-->
                           
                            <?php the_post() ?>
                             <?php the_content() ?>
                                 
                           
<!--                        </ul>-->

                    </div>

                </div>

            </div>
        </div>
    </section>

<?php get_footer(); ?>

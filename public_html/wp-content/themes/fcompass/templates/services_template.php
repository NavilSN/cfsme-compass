<?php 

/*
   Template Name: Services main page
*/   

get_header(); 
?>


    <section id="banner" class="about-us-banner service-banner">
        <div class="container">
            <div class="go-down">
                <a class="go_down_anchor" href="#service"><img class="animated fadeIn infinite" src="<?php echo get_template_directory_uri(); ?>/img/down-arrow.png" alt=""></a>
            </div>
            <div class="banner_text">
                <h1 class="avenir-bold font-38 font-0d75ad text-uppercase">Here you can see our Solutions</h1>
              
            </div>
        </div>
    </section>    


    <section id="service" class="reveal_about about_us_top_text">
        <div class="container">
            <div class="clearfix wow lightSpeedIn">
                <div class="col-md-12">
                    <div class="title_block">
                        <p class="section_subtitle font-18 font-bd8a27 avenir-medium-cond">Our</p>
                        <h2 class="section_title avenir-demi font-38 font-212121">
                            <span class="first_letter">P</span>rofessional Services
                        </h2>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="corporate-det" class="service-list">
        <div class="container">
         <?php
          $args = array(
              'hide_empty' => false
          );
          $category = get_terms( 'professional_service', $args );
          $service_count = 1 ;
          foreach ($category as $catVal) { 
		 
           ?>
            <div class="<?php echo ($service_count != 2) ? "clearfix" : " "?>  service-main">   
                <!-- content of fourth service  -->
                <div class="col-md-6 col-xs-12 col-sm-6 pull-left" <?php if($service_count <4 ) echo "style ='display: none';" ?>  >
                    <div class="content-box">
                        <h3 class="avenir-demi font-20 font-343434 text-uppercase m-t-0"><?php echo $catVal->name ; ?>  </h3>
                        <p class="font-343434 font-16 line-h-24 services">

                            <div class="excerpt">
                                <?php echo wp_trim_words( $catVal->description , 30, '...' ); ?>
                                <br>
                                <?php if ( !empty( $catVal->description ) ){ ?>
									<a href="" class="read  avenir-regular font-20 font-343434">Read More</a>
								<?php } ?>		
                            </div>
                            <div class="content">
                                <?php echo $catVal->description ; ?>
                                <br>
								<?php if ( !empty( $catVal->description ) ){ ?>
                                <a href="##" class="read-less  avenir-regular font-20 font-343434" >Close</a>
								<?php } ?>
                            </div>
                        </p>
                    </div>
                </div>

              <!-- show images of services-->              
                <div class="col-md-6 col-xs-12 col-sm-6 pull-left">
                  
                        <div class="img-box wow flipInX center">                        
                        <?php 
                        $cat_id = $catVal->term_id;
						            $thumbnail = get_field('service_image', $catVal->taxonomy . '_' . $catVal->term_id);						?>
                        <img src="<?php echo $thumbnail;?>" alt="service-image">

                        <a href="javascript:void(0)" class="<?php if ($service_count == 2 || $service_count == 3) { echo "insurance-title";} else { if ($service_count == 1) {echo "service-link";} else{echo "service-wealth-link";} }?>  avenir-demi font-20 font-ffffff text-uppercase"> <?php echo $catVal->name ; ?></a>
                        
                        <div class="<?php if ($service_count == 2 || $service_count == 3) { echo "imgbox_hover_block";} else{ echo "btn-none";}?>  ">
                            <a href="<?php echo get_category_link($catVal->term_id); ?>" class="btn btn-more  avenir-regular font-14 font-ffffff">
                            More </a>
                            <a href="<?php echo get_category_link($catVal->term_id); ?>" class="box-anchor"></a>
                            </div>
                    </div>
                   
                </div>

                <!-- content of first service  -->
                <div class="col-md-6 col-xs-12 col-sm-6 pull-left" <?php if($service_count >1 ) echo "style ='display: none';" ?>  >
                    <div class="content-box">
                        <h3 class="avenir-demi font-20 font-343434 text-uppercase m-t-0"> <?php echo $catVal->name ; ?> </h3>
                        <p class="font-343434 font-16 line-h-24">
                           <div class="excerpt">
                                <?php echo wp_trim_words( $catVal->description , 30, '...' ); ?>
                                <br>
								<?php if ( !empty( $catVal->description ) ){ ?>
                                <a href="" class="read  avenir-regular font-20 font-343434">Read More</a>
								<?php } ?>	
                            </div>
                            <div class="content">
                                <?php echo $catVal->description ; ?>
                                <br>
								<?php if ( !empty( $catVal->description ) ){ ?>
                                <a href="#" class="read-less  avenir-regular font-20 font-343434" >Close</a>
								<?php } ?>
                            </div>
                        </p>
                        
                    </div>
                </div>                
                
              </div>
              <?php $service_count++; 
               }?>
        </div>
    </section>

<?php get_footer(); ?>


<script type="text/javascript">
    $(function () {
     $('.content').hide();
     $('a.read').click(function () {
         $(this).parent().hide();
         $(this).parent().next().show();
         return false;
     });
     $('a.read-less').click(function () {
         $(this).parent().hide();
         $(this).parent().prev().show();
         return false;
     });
 });
</script>





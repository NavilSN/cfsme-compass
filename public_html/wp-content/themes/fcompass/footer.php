<footer class="bg-212121">
		<div class="container">
			<div class="row">
				<div class="col-sm-3 col-md-3">
					<?php
                    if(is_active_sidebar('footer-1')){
                    	dynamic_sidebar('footer-1');
                    }
                    ?>
				</div>
				<div class="col-sm-5 col-md-6">
					<?php
                    if(is_active_sidebar('footer-2')){
                    dynamic_sidebar('footer-2');
                    }
                    ?>
				</div>
<!--
				<div class="col-sm-6">
					<?php
                    //if(is_active_sidebar('footer-3')){
                    // dynamic_sidebar('footer-3');
                  //  }
                    ?>
-->

<!--
					<div class="copyright_container">
					</div>
-->
<!--				</div>-->
				<div class="col-sm-4 col-md-3">
					<p class="widget_title avenir-demi font-0d75ad font-14 text-uppercase subscribe">subscribe</p>

					<div class="newsletter_form">
					<?php
                        if(is_active_sidebar('footer-4')){
                        	dynamic_sidebar('footer-4');
                        }
                        ?>
					</div>
				</div>
			</div>
		</div>
	</footer>
	<script src="<?php echo get_template_directory_uri(); ?>/js/jquery-3.2.1.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/main.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/owl.carousel.min.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/scroll-reveal.min.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/wow.min.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/magnific-popup.min.js"></script>
	
	<script src="<?php echo get_template_directory_uri(); ?>/js/responsive-tabs.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/masonry.pkgd.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/imagesloaded.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/classie.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/AnimOnScroll.js"></script>
     <script>
        new AnimOnScroll(document.getElementById('grid'), {
            minDuration: 0.4,
            maxDuration: 0.7,
            viewportFactor: 0.2
        });

    </script>

	<script>
        function openNav2() {
			document.getElementById("myNav2").style.width = "100%"
		}

		function closeNav2() {
			document.getElementById("myNav2").style.width = "0%";
		}
		$(document).ready(function() {
			$('.owl-carousel').owlCarousel({
				loop: true,
				dots: true,
				responsiveClass: true,
				responsive: {
					1000: {
						items: 2,
						nav: true,
						loop: false,
						dots: true,
					},
					600: {
						items: 1,
						nav: true,
						loop: false,
						dots: true,
					},
					320: {
						items: 1,
						nav: true,
						loop: false,
						dots: true
					}

				}
			})
		});

		$(document).ready(function() {
			$('.latest_news_container').owlCarousel({
				dots: true,
				responsiveClass: true,
				responsive: {
					1000: {
						items: 1,
						nav: true,
						loop: false,
						dots: true,
					},
					600: {
						items: 1,
						nav: true,
						loop: false,
						dots: true,
					},
					320: {
						items: 1,
						nav: true,
						loop: false,
						dots: true
					}
				}
			})
		});

		jQuery(".nav a").on("click", function() {
			jQuery("#nav-menu").removeClass("in").addClass("collapse")
		});
		jQuery('#search-icon').click(function() {
			jQuery('#search-form').animate({
				duration: 300
			}).fadeToggle();
			$(this).find('img').toggleClass('displayblock displaynone');
		});

		wow = new WOW({
            boxClass: 'wow',
            animateClass: 'animated',
            offset: 0,
            mobile: true,
            live: true
        })
        wow.init();

        window.sr = ScrollReveal();

        sr.reveal('.service_container', {
            duration: 500,
            reset: true,
            mobile: false
        });
        sr.reveal('.general_insurance_container', {
            duration: 600,
            reset: true,
            mobile: false
        });
        sr.reveal('.life_insurance_container', {
            duration: 700,
            reset: true,
            mobile: false
        });
        sr.reveal('.wealth_container', {
            duration: 800,
            reset: true,
            mobile: false
        });
        sr.reveal('#latest_news', {
            duration: 800,
            reset: true,
            mobile: false
        });
        sr.reveal('.reveal_block', {
            duration: 800,
            reset: true,
            mobile: false
        }, 100);

        $('.popup_link').magnificPopup({
            type: 'image'

        });

		$(document).ready(function() {

			$(".go_down_anchor").on('click', function(event) {
				if (this.hash !== "") {
					event.preventDefault();
					var hash = this.hash;
					$('html, body').animate({
						scrollTop: $(hash).offset().top
					}, 800, function() {
						window.location.hash = hash;
					});
				}
			});
		});


		jQuery('#search-icon').click(function() {
			jQuery('.search-form').toggleClass('displayblock');
		});

		"use strict";
		! function() {
			var e = document.body,
				n = document.getElementsByClassName("b-menu")[0],
				t = document.getElementsByClassName("b-container")[0],
				s = document.getElementsByClassName("b-nav")[0];
			n.addEventListener("click", function() {
				[e, t, s].forEach(function(e) {
					e.classList.toggle("open")
				})
			}, !1)
		}();

		jQuery(document).ready(function() {
			$('.floated-btn').click(function() {
				$('.popup-menu-container .form').css('width', '100%');
			});
		});

		

		function headerHide() {
			var lastScrollTop = 0;

			window.addEventListener("scroll", function() {
				var st = window.pageYOffset;
				if (st > lastScrollTop) {
					// downscroll code
					jQuery('header').css('top', '-110px');
					jQuery('.company, .services').css('pointer-events', 'none');
					jQuery('.do-main-menu.minified').css('background-color', 'rgba(255,255,255,0.9)');
				} else {
					// upscroll code
					jQuery('header').css('top', '0px');
					jQuery('.do-main-menu.minified').css('background-color', 'rgba(255,255,255,1)');
					jQuery('.company, .services').css('pointer-events', 'all');

				}
				lastScrollTop = st;
			}, false);
		}
		
		jQuery(document).ready(function(e) {
			setInterval(headerHide(), 5000);
		});

	</script>
	<script>
        $('.btn-expand').click(function() { //you can give id or class name here for $('button')
            $(this).text(function(i, old) {
                return old == 'Read More +' ? 'Read Less -' : 'Read More +';
            });
        });
    </script>
	<?php wp_footer(); ?>
</body>

</html>

